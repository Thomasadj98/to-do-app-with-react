import React from 'react'
import TodoItem from '../TodoItem/TodoItem'

const TodoList = ({ todos, setTodos, filteredTodos }) => {
    return(
        <div className="todo-container">
            <ul className="todo-list">
                {filteredTodos.map(todo => (
                    <TodoItem 
                        todos={todos} 
                        setTodos={setTodos} 
                        todoItem={todo}
                        text={todo.text} 
                        key={todo.id}
                    />
                ))}
            </ul>
        </div>
    )
}

export default TodoList