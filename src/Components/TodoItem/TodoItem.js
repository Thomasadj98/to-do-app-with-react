import React from 'react'

const TodoItem = ({text, todoItem, todos, setTodos}) => {
    const deleteHandler = () => {
        setTodos(todos.filter(el => el.id !== todoItem.id));
    };

    const completeHandler = () => {
        setTodos(todos.map((item) => {
            if(item.id === todoItem.id){
                return {
                    ...item, completed: !item.completed
                }
            } 
            return item;
        }))
    }

    return(
        <div className="todo">
            <li className={`todo-item ${todoItem.completed ? "completed" : ""}`}>{text}</li>
            <button 
                onClick={completeHandler} 
                className="todo-button check">
            </button>

            <button 
                onClick={deleteHandler} 
                className="todo-button delete">
            </button>
        </div>
    );
};

export default TodoItem