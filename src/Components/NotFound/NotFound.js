import { Link } from "react-router-dom"

const NotFound = () => {
    return (
        <main>
            <h1>Woops, sorry it seems like this page does not exist.</h1>
            <Link to="/">Take me back Home</Link>
        </main>
    )
}

export default NotFound