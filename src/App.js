import './App.css'
import React, { useState, useEffect } from 'react'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'
import Login from './Components/Login/Login'
import NotFound from './Components/NotFound/NotFound'
import Form from './Components/Form/Form'
import TodoList from './Components/TodoList/TodoList'

function App() {
  // USE STATE
  const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState("All");
  const [filteredTodos, setFilteredTodos] = useState([]);

  // USE EFFECT
  useEffect(() => {
    getLocalTodos();
  }, []);

  useEffect(() => {
    filterHandler();
    saveLocalTodos();
  }, [todos, status]);

  // FUNCTIONS
  const filterHandler = () => {
    switch(status){
      case 'completed':
        setFilteredTodos(todos.filter(todo => todo.completed === true))
        break;
        case 'uncompleted':
          setFilteredTodos(todos.filter(todo => todo.completed === false))
          break;
        default:
          setFilteredTodos(todos)
          break;
    }
  }

  const saveLocalTodos = () => {
    localStorage.setItem('todos', JSON.stringify(todos));
  }

  const getLocalTodos = () => {
    if(localStorage.getItem('todos') === null) {
      localStorage.setItem('todos', JSON.stringify([]));
    } else {
      let local = JSON.parse(localStorage.getItem('todos'))
      setTodos(local);
    }
  }

  return (
    <BrowserRouter>
    
      <div className="App">
        <h1>Todo List</h1>

        <Form 
          todos={todos} 
          setTodos={setTodos} 
          inputText={inputText} 
          setInputText={setInputText}
          setStatus={setStatus} 
        />
        
        <TodoList 
          todos={todos} 
          setTodos={setTodos}
          filteredTodos={filteredTodos} 
        />

        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="*" component={ NotFound } />
        </Switch>
      </div>

    </BrowserRouter>
  )
}

export default App
